# Web Workers

## Task

### Please take the following steps:
---

- Implement the functionality to send user click events (clicks on buttons, the email input field) to a web worker.
- The worker must collect data into the batches of 5 items and send it to the server (using POST /analytics/user endpoint)
- For the server, use the ```personal-website-server``` which you should have already forked for the ["Using the Fetch API"](https://gitlab.com/iks3000/ajax) task.
