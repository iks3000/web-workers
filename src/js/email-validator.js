const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

const validate = (email) => {
    const inputEnding = email.substring(
        email.indexOf("@") + 1
    );
    return VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
}

const validateAsync = email => {
    const inputEnding = email.substring(email.indexOf("@") + 1);
    const valid = VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);

    return new Promise((resolve, reject) => {
        if (!valid) reject(false);
        resolve(true);
    });
}

const validateWithThrow = email => {
    const inputEnding = email.substring(email.indexOf("@") + 1);
    const valid = VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);

    if (valid) true;
    throw new Error('email is invalid');
}

const validateWithLog = email => {
    const inputEnding = email.substring(email.indexOf("@") + 1);

    console.log(inputEnding);
    return VALID_EMAIL_ENDINGS.some((value) => value === inputEnding);
}

module.exports = { validate, validateAsync, validateWithThrow, validateWithLog };