import { hamburger } from "./hamburger";
import { events, changeContentStandard, changeContentAdvance, remove } from "./join-us-section";
import { getUser } from './get-users';
import WebsiteSection from './web-component';
import { sendClickData } from './send-click-data';

hamburger();
events();
changeContentStandard();
// changeContentAdvance();
// remove();

getUser();
sendClickData();




