const assert = require('assert');
const { validate, validateAsync, validateWithThrow, validateWithLog } = require('../js/email-validator.js');

describe('Validate email', () => {
    describe('#validate()', () => {
        it('should return false if empty', () => {
            assert.strictEqual(validate(''), false);
            assert.strictEqual(validate('     '), false);
        })

        it('should return false if invalid email', () => {
            assert.strictEqual(validate('111'), false);
            assert.strictEqual(validate('abc'), false);
            assert.strictEqual(validate('111abs'), false);
        })

        it('should return false if invalid ending', () => {
            assert.strictEqual(validate('test@inbox.com'), false);
            assert.strictEqual(validate('test@test.com'), false);
            assert.strictEqual(validate('test@yandex.com'), false);
            assert.strictEqual(validate('@test@gmail.com'), false);
        })

        it('should return true if valid email ending', () => {
            assert.strictEqual(validate('test@gmail.com'), true);
            assert.strictEqual(validate('test@outlook.com'), true);
            assert.strictEqual(validate('test@yandex.ru'), true);
        })
    });

    describe('#validateAsync()', () => {
        it('handles Promise rejection', async () => {
            await validateAsync('')
                .then(() => {
                    console.log("Promise Resolved"); // ???
                }).catch((error) => {
                    console.log("Promise rejected"); // ???
                });
        })
    });

    describe('#validateWithThrow()', () => {
        it('Validate Throw Error', () => {
            assert.throws(() => validateWithThrow('test@gmail.com'), Error); // ?????
        });
    });


    // ????
    describe('#validateWithLog()', () => {
        it('Validate Throw Error', () => {
            assert.strictEqual(validateWithLog('test@gmail.com'), true);
        });
    });
})